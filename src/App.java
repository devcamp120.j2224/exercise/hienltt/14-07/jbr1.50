import com.devcamp.Account;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("--------------");
        Account account1 = new Account("KH01", "Nguyen Van A");
        Account account2 = new Account("KH02", "Tran Van B", 1000);
        //tăng số dư tài khoản
        account1.credit(2000);
        account2.credit(3000);
        System.out.println("Before:");
        System.out.println(account1.toString());
        System.out.println(account2.toString());
        //ghi nợ
        // account1.debit(1000);
        // account2.debit(5000);
        //chuyển tiền lần 1 từ 1 sang 2
        account1.debit(2000);
        account2.credit(2000);
        //in ra console
        System.out.println("After 1:");
        System.out.println(account1.toString());
        System.out.println(account2.toString());
        //chuyển tiền lần 2 từ 2 sang 1
        account1.credit(2000);
        account2.debit(2000);
        //in ra console
        System.out.println("After 2:");
        System.out.println(account1.toString());
        System.out.println(account2.toString());

        System.out.println("--------------");
    }
}
