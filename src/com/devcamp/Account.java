package com.devcamp;

public class Account {
    String id;
    String name;
    int balance = 0;

    public Account(String id, String name){
        this.id = id;
        this.name = name;
    }
    public Account(String id, String name, int balance){
        this.id = id;
        this.name = name;
        this.balance = balance;
    }
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getBalance() {
        return balance;
    }

    public int credit(int amount){
        return this.balance += amount; //cộng dồn số tiền vào số dư tài khoản
    }

    public int debit(int amount){
        String thongBao = "Amount exceeded balance";
        if(amount <= this.balance){
            this.balance = this.balance - amount;
        } else {
            System.out.println(thongBao);
        }
        return this.balance;     
    }
    @Override
    public String toString(){
        return "Account[id = " + this.id + ", nam = " + this.name + ", balance = " + this.balance + " VND]";
    }
}
